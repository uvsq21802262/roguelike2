package uvsq.m1info.roguelike.agents;

import java.util.List;

import uvsq.m1info.roguelike.level.LevelUpController;
import uvsq.m1info.roguelike.map.Point;
import uvsq.m1info.roguelike.map.Tile;

public class CreatureAi {
    protected Creature creature;

    public CreatureAi(Creature creature){
        this.creature = creature;
        this.creature.setCreatureAi(this);
    }
    
    public void onEnter(int x, int y, int z, Tile tile){
        if (tile.isGround()){
             creature.setX(x);
             creature.setY(y);
             creature.setZ(z);
        } else {
        	creature.doAction("bump into a wall");
        }
    }

    public void wander(){
    	Point p = new Point(creature.getX() , creature.getY() , creature.getZ());
    	List<Point> n = p.neighborsN(1);
    	for (Point neighbor : n){
            Creature other = creature.creature(creature.getX() + neighbor.x, creature.getY() + neighbor.y, creature.getZ());
    		if(other == null || other.glyph() != creature.glyph()) {
    			creature.moveBy(neighbor.x, neighbor.y, 0);
    			return;
    		}
    	}
    }
    
	public void onGainLevel() {
	    new LevelUpController().autoLevelUp(creature);
	}

	public void onUpdate() { }

	public void onNotify(String message){ }
}