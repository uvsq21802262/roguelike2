package uvsq.m1info.roguelike.agents;

import uvsq.m1info.roguelike.util.StuffFactory;

public class FungusAi extends CreatureAi {
	private final int MAXSPREAD = 2;
	private final double SPREADRATE = 0.00;
	private final int SPREADRANGE = 3;
    private StuffFactory factory;
    private int spreadcount;
 
    public FungusAi(Creature creature, StuffFactory factory) {
        super(creature);
        this.factory = factory;
        spreadcount = 0;
    }

    public void onUpdate() {
        if (spreadcount < MAXSPREAD && Math.random() < SPREADRATE)
            spread();
    }
    
    private void spread() {
        int x = creature.getX() + (int)(Math.random() * 2 * SPREADRANGE) - SPREADRANGE;
        int y = creature.getY() + (int)(Math.random() * 2 * SPREADRANGE) - SPREADRANGE;
  
        if (!creature.canEnter(x, y, creature.getZ()))
            return;
        creature.doAction("spawn a child");
        Creature child = factory.newFungus(creature.getZ());
        child.setX(x);
        child.setY(y);
        spreadcount++;
    }
}