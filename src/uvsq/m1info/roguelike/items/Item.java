package uvsq.m1info.roguelike.items;

import java.awt.Color;

import uvsq.m1info.roguelike.map.Location;

public class Item implements Location {

    private char glyph;
    private Color color;
    private String name;
    private int foodValue;
    private Location location;
    private int attackValue;
    private int defenseValue;
    


    public Item(char glyph, Color color, String name){
        this.glyph = glyph;
        this.color = color;
        this.name = name;
        this.location = null;
        this.foodValue = 0;
    }

    public char glyph() { return glyph; }

    public Color color() { return color; }

    public String name() { return name; }
    
    public void setLocation(Location l) { location = l; }
    
	public Location location() { return location; }

    public int foodValue() { return foodValue; }
	
    public void modifyFoodValue(int amount) { foodValue += amount; }

    public int attackValue() { return attackValue; }

    public void modifyAttackValue(int amount) { attackValue += amount; }

    public int defenseValue() { return defenseValue; }
    
    public void modifyDefenseValue(int amount) { defenseValue += amount; }
    
}