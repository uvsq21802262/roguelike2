package uvsq.m1info.roguelike.map;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class Point implements Location {
    public int x;
    public int y;
    public int z;

    public Point(int x, int y, int z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        result = prime * result + z;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
         return true;
        if (obj == null)
         return false;
        if (!(obj instanceof Point))
         return false;
        Point other = (Point) obj;
        if (x != other.x)
         return false;
        if (y != other.y)
         return false;
        if (z != other.z)
         return false;
        return true;
    }

    public List<Point> neighbors8(){
        List<Point> points = new ArrayList<Point>();
      
        for (int ox = -1; ox < 2; ox++){
            for (int oy = -1; oy < 2; oy++){
                if (ox == 0 && oy == 0)
                    continue;
                points.add(new Point(x+ox, y+oy, z));
            }
        }
        Collections.shuffle(points);
        return points;
    }
    
    public List<Point> neighborsN(int n){
        List<Point> points = new ArrayList<Point>();

        for (int i = -n; i < n + 1; i++){
            for (int j = -n; j < n + 1; j++){
                if (i == 0 && j == 0)
                    continue;
                points.add(new Point(i, j, z));
            }
        }
        Collections.shuffle(points);
        return points;
    }
    public String toString() {
    	return "(" + x + ","+ y + "," + z + ")";
    }

	public Location location() {
		return this;
	}
	
	public static List<Point> liste(int x1, int x2, int y1, int y2, int z) {
		if (x1 > x2 || y1 > y2) 
			return null;
		List<Point> liste = new ArrayList<Point>();
		for (int x = x1; x < x2; x++) {
			for (int y = y1; y < y2; y++) {
				liste.add(new Point(x, y, z));
			}
		}
		Collections.shuffle(liste);
		return liste;
	}
}
