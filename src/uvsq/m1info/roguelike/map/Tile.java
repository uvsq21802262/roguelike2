package uvsq.m1info.roguelike.map;
import java.awt.Color;
import asciiPanel.AsciiPanel;

public enum Tile {
    FLOOR((char)250, AsciiPanel.yellow),
    WALL((char)177, AsciiPanel.brightMagenta),
    BOUNDS('x', AsciiPanel.brightBlack),
    STAIRS_DOWN('v', AsciiPanel.white),
    STAIRS_UP('^', AsciiPanel.white);

    Tile(char glyph, Color color){
        this.glyph = glyph;
        this.color = color;
    }
    
    private char glyph;
    private Color color;
    
    public char glyph() { return glyph; }

    public Color color() { return color; }

    public boolean isDiggable() { return this == Tile.WALL; }

	public boolean isGround() {	return this == Tile.FLOOR; }
	
	public boolean isStairs() {	return this == Tile.STAIRS_DOWN || this == Tile.STAIRS_UP; } // pour que seul player puisse aller sue stair ( visibilité )
}