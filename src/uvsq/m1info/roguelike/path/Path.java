package uvsq.m1info.roguelike.path;

import java.util.List;

import uvsq.m1info.roguelike.agents.Creature;
import uvsq.m1info.roguelike.map.Point;


public class Path {

	  private static PathFinder pf = new PathFinder();

	  private List<Point> points;
	  public List<Point> points() { return points; }

	  public Path(Creature creature, int x, int y){
	      points = pf.findPath(creature, 
	                           new Point(creature.getX(), creature.getY(), creature.getZ()), 
	                           new Point(x, y, creature.getZ()), 
	                           300);
	  }
	}