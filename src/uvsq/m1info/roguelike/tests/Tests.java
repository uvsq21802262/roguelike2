package uvsq.m1info.roguelike.tests;

import static org.junit.Assert.*;


import org.junit.Test;
import org.junit.Before;
import uvsq.m1info.roguelike.main.ApplicationMain;
import uvsq.m1info.roguelike.screens.PlayScreen;

public class Tests {
	static PlayScreen screen;
	@Before
	public void setUp() throws Exception {
		screen = new PlayScreen();
	}
	
	@Test
	public void testIsAlive() {
		assertTrue(screen.getPlayer().isAlive());
	}

	@Test
	public void testStartLevel() {
		assertEquals(screen.getPlayer().level(), 1);
	}
	
	@Test
	public void testMaxHp() {
		assertEquals(screen.getPlayer().maxHp(), 10);
	}
	
	@Test
	public void testAttackValue() {
		assertEquals(screen.getPlayer().getAttack(), 20);
	}
	
	@Test
	public void testDefenseValue() {
		assertEquals(screen.getPlayer().getDefense(), 5);
	}


}
